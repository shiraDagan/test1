<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // Get the currently authenticated user's ID...
    
      
        //get all todos for user 1
       // $id=1; //assuming that user 1 is logged in
        //$todos = Todo::all();
        $id = Auth::id();
        //$customers = User::find($id)->customers;
        $customers = Customer::all();
        return view('customers.index', ['customers'=>$customers,'id' =>$id]);

       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //$id = 1;
        $customer = new Customer();
        $id =Auth::id();
        $boss = DB::table('users')->where('id',$id)->first();
        $username = $boss->name;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->status=0;
        $customer->user_id = $id;
        $customer->username = $username;
        $customer->save();
        return redirect('customers');  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        if (Gate::denies('admin')and ($customer->user_id != $id)) {
            abort(403,"Sorry you do not hold permission to edit this customer");
        }
        
        $id = Auth::id();
        return view('customers.edit', compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->update($request -> all());
        return redirect('customers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete customers..");
        }
        $customer = Customer::findOrFail($id);
        // if(!$books->user->id == Auth::id()) return(redirect('books'));
         $customer->delete(); 
         return redirect('customers');   
         }

         public function change_status($id,$status,Request $request)
         {
            if (Gate::denies('admin')) {//אם אתה לא מנהל
                abort(403,"Sorry you are not allowed to change status..");
            }
             $customer = Customer::find($id);
             $customer->status = 1;//האינדקס יישלח אותנו לפונקציה הזו ויעדכן את הסטטוס ל 1 
             $customer->update($request -> all());//מעדכן את הדטה בייס
             return redirect('customers');//טעינה מחודשת של דף האינדקס כאשר עכשיו הסטטוס הוא 1
         }

    }

