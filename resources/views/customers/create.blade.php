@extends('layouts.app')
@section('content')

<h1>Add a new customer</h1>
<form method = 'post' action = "{{action('CustomerController@store')}}" >
{{csrf_field()}}      

   
<div class = "form-group">    
    <label for = "name">Name </label>
    <input type = "text" class = "form-control" name = "name">

    <label for = "email">Email </label>
    <input type = "text" class = "form-control" name = "email">

    <label for = "phone">Phone </label>
    <input type = "text" class = "form-control" name = "phone">

</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>

</form>
@endsection

