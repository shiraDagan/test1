@extends('layouts.app')
@section('content')

<h1>This is a customers list</h1>
 <h4><a href = "{{route('customers.create')}}">   create a new customer </a></h4>

<table class = 'table'>
 <thead>  
      <tr >  
      <th>name</th> 
      <th>email</th> 
      <th>phone</th>
      <th>created_by</th>
      <th>edit</th>
      <th >Delete</th>
       </tr>  
  </thead>
  
  @foreach($customers as $customer)
  <!-- <tr>           למקרה שנרצה שרק השם יסומן בירוק, אחרת מיותר-->  
         @if ($customer->status)   

              <tr style="background-color: green">
              <!-- <td  style="background-color: green" >{{$customer->name }}</td> -->
         @else 
               <tr>
               <!-- <td  >{{$customer->name }}</td> -->
              
        @endif  
                 @if ($customer->user_id == $id)
                 <td style="font-weight:bold">{{$customer->name}}</td>
                 @else
                <td>{{$customer->name}}</td>  
                 @endif 
 
                            <td  >{{$customer->name }}</td>
                            <td  >{{$customer->email }}</td>
                            <td  >{{$customer->phone }}</td>
                            <td  >{{$customer->username }}</td>
                            <td  ><a href ="{{route('customers.edit', $customer ->id)}}" >edit</a></td>
                            <td  >@can('admin') <a href="{{route('delete', $customer->id)}}">@endcan Delete</a></td>
                                    
                            @cannot('salesrep')
                                @if ($customer->status)        
                                 @else
                                <td>  <a href="{{route('customers.change_status', [ $customer->id, $customer->status ])}}">deal closed</a> </td>

                                @endif
                            @endcannot     


                            </tr>
     @endforeach
                            </table>


@endsection